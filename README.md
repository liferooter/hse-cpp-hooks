# HSE C++ pre commit hooks

This repository provides [pre-commit](https://pre-commit.com/) hooks for HSE AMI C++ course task submission.

## Example usage

Add following lines to your `.pre-commit-config.yaml`:

```yaml
repos:
  - repo: https://gitlab.com/liferooter/hse-cpp-hooks
    rev: v1.1.0
    hooks:
      - id: no-extra-files
      - id: test-task
```

## Hooks

- `no-extra-files` hook checks that only changes of `tasks/<task name>/<task name>.cpp` and new files in `tasks/<task name>/` are committed to `submits/<task name>` branch.

- `test-task` hook tests the task before commits on `submits/<task name>` branches. It requires `./build` directory to be a configured `cmake` build directory.

